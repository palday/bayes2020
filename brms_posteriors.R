#' ---
#' title: MCMC Examples; Posteriors and Priors with `brms`
#' subtitle: And why good priors are really good and bad priors aren't really a problem
#' author: Phillip M. Alday
#' date: 5 July 2018
#' output:
#'    html_document:
#'       toc: true
#'       toc_float: true
#' ---

library("tidyverse")  # for plotting and data manipulation
library("ggrepel")    # for plotting text labels that don't overlap
library("ggridges")   # for rows of density plots
library("brms")       # for modelling
library("HDInterval") # for HDIs

rstan::rstan_options(autowrite=TRUE)
# most modern computers have at least cores, so this should speed things up
options(mc.cores=2)
# my preferred ggplot theme
theme_set(theme_light())
#' We'll be using the `msleep` dataset on mammalian sleep patterns (available as
#' part of the `tidyverse`).
data(msleep)
sleepy <- msleep

#' Setting our random seed explicitly ensures that we'll get the same results
#' each time we run this script.
set.seed(42)

#' We label a random sample of the dataset, and make sure that we label humans.
samp <- sample(nrow(sleepy),15)
sleepy$label <- ""
sleepy$label[samp] <- sleepy$name[samp]
sleepy$label[sleepy$name == "Human"] <- "Human"

sleepy %>%
    ggplot(aes(log10(brainwt),sleep_total)) +
    geom_point(color=ifelse(sleepy$name == "Human", "red", "black"),show.legend = FALSE) +
    geom_text_repel(aes(label=label)) +
    scale_x_continuous(breaks=log10(c(0.001,0.1,0.5,1,2,5)),labels=function(x) 10^x) +
    labs(x="Brain Weight (kg, log scale)",
         y="Total amount of sleep (hours)",
         title="The Weight of Sleep")
g.sleepy <- last_plot()

#' # Posteriors
#'

#+ initial.model, cache=TRUE, message=FALSE
model <- brm(sleep_total ~ 1 + log10(brainwt),
             data=sleepy,
             family=gaussian(),
             algorithm="sampling", # other options are "meanfield" and "fullrank" for variational approximations
             sample_prior=TRUE,    # this is needed for plotting hypotheses
             save_all_pars=TRUE,   # this is needed for plotting hypotheses
             chains=4,
             iter=6e3)

#' The model summary includes information on the model design (formula, family
#' and priors, where applicable) as well as summary statistics about the
#' posterior.  The CIs are Bayesian credible intervals.

summary(model, priors=TRUE)

#' Note that when we don't specify priors, `brms` still automatically includes
#' priors for the intercept and the residual variation (sigma). `brms` works
#' some magic on the intercept to improve sampling efficiency and so the
#' intercept is handled differently.
#'
#' There is a lot of documentation on possible priors, see `?set_prior`, and
#' especially the second paragraph of the section on population-level effects
#' for notes on the intercept.
#'
#' These minimal priors for the intercept and the residual variation are largely
#' to help model fitting and having minimal impact on the estimates. This is
#' obvious when we compare the results to the frequentist estimate, which
#' corresponds to flat priors for this simple case:

summary(lm(sleep_total ~ 1 + log10(brainwt),sleepy))

#' You can explore, plot and export your model further by running:
#'
#' ```r
#' launch_shinystan(model)
#' ```

#' In addition to checking our $\^R$ values, we should also always check that
#' our chains show no signs of autocorrelation and look like a hairy
#' caterpillar:

plot(model)

#' The model summary provides us with the 95% HDI (called "CI"), but we can also
#' calculate this with the `HDInterval::hdi` by extracting the MCMC
#' representation of the posterior.

hdi(as.mcmc(model,pars="^b_"),allowSplit=TRUE,credMass=0.95)

#' See `?hdi` for more information on the `allowSplit` setting.

#' We can also compute the quantile credible intervals ourselves this way, using
#' `apply()` to compute quantiles of each parameter.
apply(as.mcmc(model,pars="^b_",combine_chains=TRUE),
      2, # apply by columns, which in our case correspond to model parameters
      quantile,
      c(0.025, 0.975) # For a 95% quantile credible interval,
      )                # we need the 2.5% and and the 97.5% quantiles

#' `brms` also provides a convenience function for doing this:

posterior_summary(model,pars = "^b_", robust=FALSE)

#' Note that if you set `robust=TRUE`, then the estimate is the median (and the
#' error is the median absolute deviation instead of the standard deviation):
posterior_summary(model,pars = "^b_", robust=TRUE)


#' Pictures speak louder than words, so I like to present coefficient plots in
#' my papers, with the full model tables pushed back into appendices or
#' supplementary materials (but still easily available!).

posterior_samples(model, pars="^b_",add_chain = TRUE) %>%
    as_tibble() %>%
    gather(key="coef",value="estimate",1:2) %>%
    ggplot(aes(y=coef,x=estimate)) +
        geom_density_ridges(aes(fill=chain),
                            show.legend = FALSE,
                            alpha=0.2)

#' We can also combine these plots with $\^R$ values and markers for either the
#' quantile or HDI. Here is a more extensive example.

post_full <- posterior_samples(model, pars="^b_",add_chain = TRUE) %>%
    as_tibble() %>%
    gather(key="coef",value="estimate",1:2) %>%
    mutate(coef = str_replace(coef,"^b_",""))

post_sum <- posterior_summary(model,pars = "^b_",
                              robust=FALSE,
                              probs=c(0.025,0.15, 0.5, 0.75,0.975)) %>%
            as_tibble(rownames=NA) %>%
            rownames_to_column("coef") %>%
            mutate(coef = str_replace(coef,"^b_",""))

ggplot(post_full, aes(y=coef,x=estimate)) +
        geom_density_ridges(aes(fill=chain),
                            show.legend = FALSE,
                            alpha=0.2) +
        geom_errorbarh(aes(x=Estimate, xmin=Q15, xmax=Q75, linetype="60% Quantile"),
                       height=0.2, size=2, data=post_sum) +
        geom_errorbarh(aes(x=Estimate, xmin=Q2.5, xmax=Q97.5, linetype="95% Quantile"),
                height=0.1, size=1, data=post_sum) +
        labs(y="Coefficient",
             x="Posterior Estimate",
             title="Model coefficients",
             subtitle="Posterior distibutions and quantile intervals")

#' It's also important to access how well our model fits the observed pattern of
#' data. There are two ways to do this. The posterior predictive check uses
#' draws from the posterior distribution to generate new data. The distribution
#' of this new data is then compared to the observed distribution.

pp_check(model,nsamples=30) + labs(title="Posterior predictive check",
                       x="Total Sleep Time (hours)",
                       y="density")
#' `pp_check` has many more options for other types of plots or splitting the
#' predictive distribution by conditions, etc. Make sure to check out
#' `?pp_check`.
#'
#' As in frequentist models, we can also plot the fitted model against the
#' observed data. We plot the lines resulting from single draws from the
#' posterior distribution as well as a darker line representing the summary
#' statistics (in this case, the mean) of the posterior distribution. (For
#' unskewed Gaussian like we have here, the posterior mean, median and mode will
#' all yield similar results. For more complicated models, they can of course be
#' quite different.)
fitted_lines <- posterior_samples(model, pars="^b_") %>%
    as_tibble() %>%
    sample_n(30) %>%
    transmute(Intercept=b_Intercept,
              Slope=b_log10brainwt)

g.sleepy +
    geom_abline(aes(intercept=Intercept,slope=Slope),
                color="skyblue",alpha=0.4,data=fitted_lines) +
    geom_abline(intercept=fixef(model)["Intercept","Estimate"],
                slope=fixef(model)["log10brainwt","Estimate"],
                color="blue",alpha=1.0)
#' Much like the `effects` package for frequentist models, `brms` has some
#' built-in functionality for calculating and visualizing marginal effects (i.e.
#' the impact of a single predictor, when marginalizing out other predictors /
#' holding them constant at a weighted average).

plot(marginal_effects(model))
#' Note that the default calculation backtransforms any variables that were
#' transformed as part of the model formula. This is one advantage to doing
#' these transformations in the formula instead of beforehand.

head(marginal_effects(model)[[1]])

#' We can also customize the plot a bit to make it better match our previous plots.
plot(marginal_effects(model))[[1]] + scale_x_log10() +
    labs(x="Brain Weight (kg, log scale)",
         y="Total amount of sleep (hours)",
         title="The Weight of Sleep")

#' Or even combine the two plots.
plot(marginal_effects(model))[[1]] +
    geom_point(aes(brainwt,sleep_total), inherit.aes=FALSE,
               color=ifelse(sleepy$name == "Human", "red", "black"),
               show.legend = FALSE,
               data=sleepy) +
    geom_text_repel(aes(brainwt,sleep_total,label=label), inherit.aes=FALSE, data=sleepy) +
    labs(x="Brain Weight (kg, log scale)",
             y="Total amount of sleep (hours)",
             title="The Weight of Sleep") +
    scale_x_log10(breaks=c(0.001,0.1,0.5,1,2,5),
                  labels=function(x) sprintf("%s",x)) +
    labs(subtitle="Marginal effect overlaid")

#' And even add in an overlay of the frequentist fit:
last_plot() +
    geom_smooth(aes(brainwt,sleep_total),
                color="darkred",fill="darkred",
                method="lm",inherit.aes=FALSE, data=sleepy) +
    labs(subtitle="Marginal effect overlaid in blue+grey, frequentist regression in red")

#' We see that the fits are almost identical. This is because the marginal effect here is the actual entire effect and we have flat priors. The Bayesian intervals are slightly wider, but that's ultimately a good thing here, given how much of the overall variances we're still unable to explain:

bayes_R2(model)

#' # Priors
#'
#' Thus far, we've only used the default flat priors (well, except for the
#' intercept and the residual variance).  These priors are *improper priors*
#' because they don't reflect a valid probability distribution -- it's not
#' really possible for all values (here: real numbers) to be equally probable,
#' without either the probability of any given value disappearing to zero or the
#' total probability exceeding 1, (i.e. exceeding 100%), which isn't possible.
#'
#' As we'll see next week, informative priors are actually a good thing because
#'

#' 1. they encode information we're already using (gut feelings / prior experience with plausible values)
#' 2. they can help with model convergence and speed
#' 3. they can help overcome issues with multicollinearity, correlation or total
#' separation
#' 4. they provide a way to encode additional desireable assumptions (e.g. all
#' effects are small) or even constrain our models in interesting ways  (assuming
#' most effects are actually zero provides a convenient way to perform variable
#' selection)
#' 5. the impact of the prior is limited by the weight of the evidence from the
#' observed data.
#'

#' To really help make this point, we'll look at what happens with priors of
#' varying strengths. We start with a very weak normal prior centered at zero,
#' with a standard deviation of 100.

#+ model.weak.prior, cache=TRUE, message=FALSE
model.weak_prior <- update(model,
                           prior=set_prior("normal(0,100)",class="b"),
                           sample_prior=TRUE)

#' We can examine how much the weak prior shifted our estimates relative to the
#' flat priors:

fixef(model) - fixef(model.weak_prior)

#' The difference is tiny. We can make this even clearer with a plot:
model.posterior <- posterior_samples(model, pars="^b_") %>%
    as_tibble() %>%
    gather(key="coef",value="estimate",1:2) %>%
    mutate(coef = str_replace(coef,"^b_",""))
model.weak_prior.posterior <- posterior_samples(model.weak_prior, pars="^b_") %>%
    as_tibble() %>%
    gather(key="coef",value="estimate",1:2) %>%
    mutate(coef = str_replace(coef,"^b_",""))

ggplot(mapping=aes(y=coef,x=estimate)) +
    geom_density_ridges(aes(fill="Flat"),
                    alpha=0.2, data=model.posterior) +
    geom_density_ridges(aes(fill="Weak"),
                        alpha=0.2, data=model.weak_prior.posterior) +
    scale_fill_brewer(type="qual") +
    labs(y="Coefficient",
         x="Posterior Estimate",
         title="Model coefficients",
         fill="Prior",
         subtitle="The impact of different priors")
g.prior_comp <- last_plot()

#' The difference is truly imperceptible.
#'
#' We can also take advantage of the hypothesis function to show reallocation of
#' probability based on the observed data. In other words, we can show the shift
#' from our initial beliefs/state of knowledge (the prior) to our "final"
#' beliefs (the posterior).

plot(hypothesis(model.weak_prior,hypothesis="log10brainwt=0"))


#' In this particular plot, our prior is so diffuse, that's it not even visible
#' on the scale of the plot. We can constrain the plot to a smaller interval and
#' the prior will become more visible:

plot(hypothesis(model.weak_prior,hypothesis="log10brainwt=0"))[[1]] + xlim(-10,10)

#' Note that this type of plot requires the specification of a hypothesis. For
#' our use here, the important thing is that it's a hypothesis about the
#' parameter, whose prior and posterior distributions we would like to compare.
#' We can do more complicated hypotheses involving more parameters, such as

plot(hypothesis(model.weak_prior,hypothesis="log10brainwt=Intercept"))[[1]] + xlim(-10,10)

#' The relevant parameter is then a latent value, namely the difference between
#' the slope and the interept, so the corresponding prior and posterior
#' distributions of this value are then plotted. In this particular case, this
#' isn't an interesting hypothesis, but this is interesting for example when
#' comparing the relative impact of two categorical conditions.
#'
#' When not plotted, the hypothesis function also gives you a Bayesian
#' significance test of sorts for the relavant hypothesis:

hypothesis(model.weak_prior,hypothesis="log10brainwt=0")

#' The evidence ratio is a Bayes factor comparing the hypothesis (here:
#' `log10brainwt` is (exactly) zero) and its implied altenrative (here:
#' `log10brainwt` is not (exactly) zero) comuted via the Savage-Dickey method.
#' I'm generally opposed to testing so-called point hypotheses; I much prefer the ROPE
#' (region of practical equivalance) based decision rules put forth by John
#' Kruschke. The `hypothesis`` function will actually allow you to test such hypotheses, albeit still using Bayes factors instead of looking at interval overlap as Kruschke does:

hypothesis(model.weak_prior,hypothesis="abs(log10brainwt - -2.4) < 0.1")
plot(hypothesis(model.weak_prior,hypothesis="abs(log10brainwt - -2.4) < 0.1"))[[1]] +
    xlim(-10,10) +
    labs(title="Is log10(brainwt) practically equivalent to -2.4?",
         subtitle="If yes, the majority of posterior mass for this difference should be around zero")

hypothesis(model.weak_prior,hypothesis="abs(Intercept) < 0.1")
plot(hypothesis(model.weak_prior,hypothesis="abs(Intercept) < 0.1"))[[1]] +
    xlim(-10,10) +
    labs(title="Is the intercept practically equivalent to zero?",
         subtitle="If yes, the majority of posterior mass for this difference should be around zero")

#' For the medium and strong prior, we will skip any individual plots and just
#' compare their impact to the flat and weak priors.

#+ model.medium.prior, cache=TRUE, message=FALSE
model.medium_prior <- update(model,
                           prior=set_prior("normal(0,10)",class="b"),
                           sample_prior=TRUE)
#+ model.strong.prior, cache=TRUE, message=FALSE
model.strong_prior <- update(model,
                             prior=set_prior("normal(0,1)",class="b"),
                             sample_prior=TRUE)

#' We see that only the strong prior really impacts the data, pulling the
#' estimate for `log10(brainwt)` noticeably closer to zero, which has the
#' knock-on effect of increasing the estimate for the intercept.
model.medium_prior.posterior <- posterior_samples(model.medium_prior, pars="^b_") %>%
    as_tibble() %>%
    gather(key="coef",value="estimate",1:2) %>%
    mutate(coef = str_replace(coef,"^b_",""))
model.strong_prior.posterior <- posterior_samples(model.strong_prior, pars="^b_") %>%
    as_tibble() %>%
    gather(key="coef",value="estimate",1:2) %>%
    mutate(coef = str_replace(coef,"^b_",""))

g.prior_comp +
    geom_density_ridges(aes(fill="Medium"),
                    alpha=0.2, data=model.medium_prior.posterior) +
    geom_density_ridges(aes(fill="Strong"),
                        alpha=0.2, data=model.strong_prior.posterior)
g.prior_comp <- last_plot()

#' The overall model fit still is noticeably worse for the strong prior, but the
#' model is also more conservative. The estimate for the brain-weight effect is
#' smaller, as is the esimtate for the amount of variance we can explain.
bayes_R2(model.strong_prior)

#' This shrinking of effect sizes is generally a good thing, because unbiased
#' estimates (such as those from flat priors) are often overestimates (Gelman
#' and Carlin, 2014).
#'
#' And our biased estimates still do a decent job capturing the overall trend:
fitted_lines.flat <- posterior_samples(model, pars="^b_") %>%
    as_tibble() %>%
    sample_n(30) %>%
    transmute(Intercept=b_Intercept,
              Slope=b_log10brainwt)

fitted_lines.strong <- posterior_samples(model.strong_prior, pars="^b_") %>%
    as_tibble() %>%
    sample_n(30) %>%
    transmute(Intercept=b_Intercept,
              Slope=b_log10brainwt)

g.sleepy +
    geom_abline(aes(intercept=Intercept,slope=Slope,color="Flat"),
                alpha=0.4,data=fitted_lines.flat) +
    geom_abline(intercept=fixef(model)["Intercept","Estimate"],
                slope=fixef(model)["log10brainwt","Estimate"],
                color="blue",alpha=1.0,size=2) +
    geom_abline(aes(intercept=Intercept,slope=Slope, color="Strong"),
                alpha=0.4,data=fitted_lines.strong) +
    geom_abline(intercept=fixef(model.strong_prior)["Intercept","Estimate"],
                slope=fixef(model.strong_prior)["log10brainwt","Estimate"],
                color="darkred",alpha=1.0,size=2) +
    scale_color_manual(values=c("Flat"="skyblue","Strong"="red")) +
    labs(subtitle="The impact of priors",
         color="Prior")

#' Our final example is a "bad" prior. This one has a mean very far away from
#' the observed data's mean and a very small standard deviation. In other words,
#' we're encoding a very strong belief (small standard deviation) that is very
#' wrong (far away from the actual value). So it will take more convincing to
#' move us away from our prior; equivalently, we won't move as far for the same
#' evidence.

#+ model.bad.prior, cache=TRUE, message=FALSE
model.bad_prior <- update(model,
                prior=set_prior("normal(20,1)",class="b"),
                sample_prior=TRUE)

#' Now clearly, we're way off of the other estimates. But science is an
#' iterative process, so maybe we have good reasons for our strong prior
#' beliefs.

model.bad_prior.posterior <- posterior_samples(model.bad_prior, pars="^b_") %>%
    as_tibble() %>%
    gather(key="coef",value="estimate",1:2) %>%
    mutate(coef = str_replace(coef,"^b_",""))

g.prior_comp +
    geom_density_ridges(aes(fill='"Bad"'),
                        alpha=0.2, data=model.bad_prior.posterior)
g.prior_comp <- last_plot()

#' Nonetheless, we've still drastically changed our beliefs, as the
#' prior-vs.-posterior plot makes clear:

plot(hypothesis(model.bad_prior,"log10brainwt=0"))

#' The total movement here is large. Science is (ideally) an iterative process,
#' where we update our beliefs/knowledge with each bit of new evidence. So we
#' use our posteriors to as our new priors when we have more evidence. If the
#' new evidence is similar to the old evidence, then we should move even
#' further. We'll simulate this here by re-using the same dataset. (Please don't
#' do this in real life -- it's only an example here of what happens if we
#' receive new data that has similar properties.)

model.bad_prior.posterior %>%
    group_by(coef) %>%
    summarize(mean=mean(estimate),sd=sd(estimate))

#' posterior.to.prior, cache=TRUE, message=FALSE
model.bad_prior.new_evidence <- update(model.bad_prior,
                                       prior=c(set_prior("normal(17.2,1.06)",
                                                         class="b")),
                                       sample_prior=TRUE)

#' And we see that with increasing evidence, we move closer to the other values.
model.bad_prior.new_evidence.posterior <- posterior_samples(model.bad_prior.new_evidence, pars="^b_") %>%
    as_tibble() %>%
    gather(key="coef",value="estimate",1:2) %>%
    mutate(coef = str_replace(coef,"^b_",""))

g.prior_comp +
    geom_density_ridges(aes(fill='"Bad" after iterating with more evidence'),
                        alpha=0.2, data=model.bad_prior.new_evidence.posterior)

#' We can also consider the case where we had a lot more data initially with the
#' bad prior. To simulate this (again, don't do this in real life!), we can
#' simply duplicate our original dataset.
#'

#+ bad.prior.big.evidence, cache=TRUE, message=FALSE
bigdata <- bind_rows(sapply(1:2, function(x) sleepy, simplify = FALSE))

model.bad_prior.more_evidence <- update(model.bad_prior, newdata=bigdata)

model.bad_prior.more_evidence.posterior <- posterior_samples(model.bad_prior.more_evidence, pars="^b_") %>%
    as_tibble() %>%
    gather(key="coef",value="estimate",1:2) %>%
    mutate(coef = str_replace(coef,"^b_",""))

g.prior_comp +
    geom_density_ridges(aes(fill='"Bad" after iterating with more evidence'),
                        alpha=0.2, data=model.bad_prior.new_evidence.posterior) +
    geom_density_ridges(aes(fill='"Bad" with more evidence right away'),linetype="dashed",
                        alpha=0.2, data=model.bad_prior.more_evidence.posterior)

#' It's difficult to see the estimates from this new model, so I've marked them
#' with a dashed line.
#'
#' Doubling the evidence right away has a similar bigger impact to the iterative
#' approach. The total evidence we've accumulated in both cases is identical.
#' This is not, however, an excuse for low-powered experiments. We ideally
#' should be able to interpret the current state of knowledge and not have to
#' wait for somebody else to finish collecting our evidence. Having sufficient
#' power means that the evidence can overcome our assumptions if they were
#' wrong. To convince you of this, here is the same example with multiple copies
#' of the previous dataset

#+ power.to.the.model, cache=TRUE, message=FALSE
bigdata <- bind_rows(sapply(1:3, function(x) sleepy, simplify = FALSE))
model.bad_prior.triple <- update(model.bad_prior, newdata=bigdata)
model.bad_prior.triple.posterior <- posterior_samples(model.bad_prior.triple, pars="^b_") %>%
    as_tibble() %>%
    gather(key="coef",value="estimate",1:2) %>%
    mutate(coef = str_replace(coef,"^b_",""))

bigdata <- bind_rows(sapply(1:4, function(x) sleepy, simplify = FALSE))
model.bad_prior.quadruple <- update(model.bad_prior, newdata=bigdata)
model.bad_prior.quadruple.posterior <- posterior_samples(model.bad_prior.quadruple, pars="^b_") %>%
    as_tibble() %>%
    gather(key="coef",value="estimate",1:2) %>%
    mutate(coef = str_replace(coef,"^b_",""))

model.quadruple <- update(model, newdata=bigdata)
model.quadruple.posterior <- posterior_samples(model.quadruple, pars="^b_") %>%
    as_tibble() %>%
    gather(key="coef",value="estimate",1:2) %>%
    mutate(coef = str_replace(coef,"^b_",""))


ggplot(mapping=aes(y=coef,x=estimate)) +
    geom_density_ridges(aes(fill="1x",linetype="Flat"),
                        alpha=0.2, data=model.posterior) +
    geom_density_ridges(aes(fill="4x",linetype="Flat"),
                        alpha=0.2, data=model.quadruple.posterior) +
    scale_fill_brewer(type="qual") +
    labs(y="Coefficient",
         x="Posterior Estimate",
         title="Model coefficients",
         fill="Evidence",
         linetype="Prior Type",
         subtitle="The interaction of evidence and priors") +
    geom_density_ridges(aes(fill="1x",linetype='"Bad"'),
                        alpha=0.2, data=model.bad_prior.posterior) +
    geom_density_ridges(aes(fill="2x",linetype='"Bad"'),
                        alpha=0.2, data=model.bad_prior.new_evidence.posterior) +
    geom_density_ridges(aes(fill="3x",linetype='"Bad"'),
                        alpha=0.2, data=model.bad_prior.triple.posterior) +
    geom_density_ridges(aes(fill="4x",linetype='"Bad"'),
                    alpha=0.2, data=model.bad_prior.quadruple.posterior)

#' Bayes allows us to integrate previous evidence as our prior in a rigorous
#' way, which allows us to give a quantitative estimate that reflects not just
#' our own data, but the overall state of the field. That doesn't excuse us
#' however maximizing the power in our own studies.
#'
#' The moral of the story is, as always, we need more power.
#'
#' 1.21 Gigawatts, if we're going to get to the future.


