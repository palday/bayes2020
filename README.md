---
title: Bayesian Estimation and Regression
author: 
    - Phillip Alday
    - Natalia Levshina
date: March - May 2020
---

# Bayesian Estimation and Regression

Taught by Phillip Alday and Natalia Levshina.

This repository hosts the code and documentation for Phillip's portion.

[[_TOC_]]

## Course Description

### Goals
This course covers Bayesian basics in R with a particular focus on estimation within a multilevel (mixed-effect) model framework for applications in psychology and linguistics. Note that this course explicitly focuses on Bayesian statistics for repeated-measures estimation / multilevel modelling applications and not on simple Bayes-factor based replacements for classical statistical tests.

### Learning Outcomes
Students will know which models to run, how to implement them correctly, and how to report the
results, using tools in R that are largely drop-in replacements for frequentist tools (e.g. Bayesian
replacements for `lm()` and `lmer()`), based on the probabilistic language Stan. Directly programming in
Stan will not be covered in this course.

### Prerequisites
- A solid understanding of linear regression
- A passing understanding of the general linear model (e.g. at the level in the Andy Field’s popular
*Discovering Statistics* series)
- A basic understanding of mixed effects models would be useful

### About the instructors
**Phillip** has a background in the electrophysiology of language and was a mathematician in a previous
life. A bit of a contrarian, he is a Bayesian at the frequentist events and a Frequentist heckler at the
Bayesian events, and thinks that frequentist calibration matters a lot for Bayesianism and knows Bayes’
theorem is quite important for frequentists as well.

**Natalia** is a quantitative linguist. Her main motivation for using Stan is its flexibility for modelling
capricious linguistic data. She has a lot of experience in fitting generalized linear models, trying to get the best of the frequentist and Bayesian worlds.

## Format, schedule and content outline
Mix of lecture and tutorial plus exercises to fulfil 1 EC = 28 study hours. Five (5) sessions, 90 minutes
each, with an additional optional session 0 reviewing matrix notation for regression models.

### Plan

**Note that due to containment concerns related to COVID-19, in-person sessions are currently cancelled. We have moved to online Zoom sessions, with the meeting information distributed privately via email and the [GWDG RocketChat service](https://chat.gwdg.de/).**

MPI members already have a GWDG account, but external participants may need to request one, see the [documentation](https://chat.gwdg.de/) for more details. There is a private channel for course-related discussions `bayesian.2020`; please notify an instructor or Kevin Lam if you are not a member of that channel. In addition to discussion, "smaller" course materials such as slides may also be posted there. 
 
Recordings of invidual sessions will be made available to participants via GWDG's [ownCloud service](https://owncloud.gwdg.de/index.php/f/904331677); access information (password) is available on RocketChat. Links to individual recordings are available via the session number in the plan below, but you will still need a password. The recordings are not licensed for further distribution: they are only available for course participants. Note that it may take several hours to post the recordings because we edit out "dead time" at the beginning of the session and transcode them to a more mangeable resolution and file size.

|Session|Topic|Instructor|Date and location|
|---|---|---|---|
|0|Regression review -- classical linear regression and the general linear model, notation. [Slides](https://gitlab.com/palday/bayes2020/-/jobs/artifacts/master/raw/regression-review.pdf?job=regression-review).|Alday|Wednesday, 11 March 15:30 – 17:00 @ MPI 236|
|[1](https://owncloud.gwdg.de/index.php/f/904708621)|How does Bayes actually differ from frequentism? Basic concepts of probabilities. [Where do I look for distributions?](http://rpubs.com/palday/bayes) |Levshina| ~~Thursday, 12 March 15:30 – 17:00 @ MPI 163~~ Tuesday, 28 April 14:00 – 15:30 |
|2|What do Markov chains, etc. do? MCMC diagnostics How to summarize and report posteriors?|Alday and Levshina| ~~Thursday, 19 March 13:00 – 14:30 @ MPI 236~~  Wednesday, 6 May 14:00 – 15:30|
|3|Bayesian equivalents for 'standard' linear mixed-effect models in R; priors in EEG and ET research|Alday|~~Friday, 20 March 3:00 – 14:30 @ MPI 163~~ Tuesday, 12 May 14:00 – 15:30|
|4|Bayesian equivalents for Generalized linear mixed-effects models in R, priors for logistic models in language|Levshina|~~Wednesday, 25 March 15:30 – 17:00 @ MPI 163~~ Tuesday, 19 May 14:00 – 15:30|
|5|Grab bag and departing thoughts. Using the full power of Bayes  to replace 'advanced' methods (regularized regression, GAMs, changepoint  models, distributional models and heteroskedacity, multiple imputation,  errors-within-variables, ordinal models, etc.) in a single framework.  ‘Paradoxes’ of weakly informative priors. Model diagnostics review and  problem mitigation strategies.|Alday and Levshina|~~Thursday, 26 March 15:30 – 17:00 @ MPI 163~~  Tuesday, 26 May 14:00 – 15:30|


### Recommended reading

#### Books
* Downey, Allen B. (2012). Think Bayes: Bayesian Statistics Made Simple. Needham, MA: Green Tea Press. [Text and supporting materials.](https://greenteapress.com/wp/think-bayes/)
* Gelman, A., Carlin, J., Stern, H., Dunson, D., Vehtari, A. & Rubin, R. (2014). Bayesian Data Analysis. 3rd Edition. Boca Raton: CRS Press. [Text and supporting materials.](http://www.stat.columbia.edu/~gelman/book/)
* Gelman, A., & Hill, J. (2006). Data Analysis Using Regression and Multilevel/Hierarchical Models . Cambridge:
Cambridge University Press. *Note that the new "edition" of this work will be a two-part book series, due to appear in the near-ish term future*.
* Kruschke, J. K. (2011). Doing Bayesian Data Analysis: A Tutorial with R und BUGS. Academic Press.
* McElreath, R. (2016). Statistical Rethinking. CRC Press. [Related R package](https://github.com/rmcelreath/rethinking/).

#### Papers
* Nicenboim, B., & Vasishth, S. (2016). Statistical methods for linguistic research: Foundational Ideas-Part II.
Language and Linguistics Compass , 10 (11), 591–613. doi:[10.1111/lnc3.12207](https://doi.org/10.1111/lnc3.12207) [arXiv version](https://arxiv.org/abs/1602.00245).
* Schad, D. J., Betancourt, M., & Vasishth, S. (2019). Toward a principled Bayesian workflow in cognitive science. [arXiv version](https://arxiv.org/abs/1904.12765).
* Sorensen, T., Hohenstein, S., & Vasishth, S. (2016). Bayesian linear mixed models using Stan: A tutorial for
psychologists, linguists, and cognitive scientists. The Quantitative Methods for Psychology, 12(3), 175–200.
doi:[10.20982/tqmp.12.3.p175](https://doi.org/10.20982/tqmp.12.3.p175). [arXiv version](https://arxiv.org/abs/1506.06201).

#### Animations and Videos

* [Precision is the goal](https://youtu.be/lh5btlAvrLs): A very nice perspective by John Krushcke on power analysis and optional stopping within the Bayesian framework. The corresponding publication is [here](https://doi.org/10.3758/s13423-016-1221-4).
* [The Markov-chain Monte Carlo Interactive Gallery](https://chi-feng.github.io/mcmc-demo/)

#### Blogs
* [Doing Bayesian Data Analysis](https://doingbayesiandataanalysis.blogspot.com/). John Kruschke's blog.
* [Publishable Stuff: Rasmus Bååth's Research Blog](http://www.sumsar.net/).
* [While My MCMC Gently Samples](https://twiecki.io/).  Thomas Wiecki's blog -- focused more on Python, as he is one of the PyMC3 developers, but with lots of posts based on building intuition about difficult math concepts.

For practical components, laptops with R and RStan (https://github.com/stan-dev/rstan/wiki/RStanGetting-Started) installed will be required. Please note that installing RStan can take some time, so plan
accordingly. R Studio and git are also suggested. Issues installing the software must be addressed
outside of and ideally before class.

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.
Textual portions of this work are licensed under CC-BY-NA-4.0  Code portions are lincensed under 2-Clause BSD.
