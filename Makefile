# Copyright (c) 2020, Phillip Alday
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

python = python
library ?= $(BIB)
refs ?= bibliography.bib

.DEFAULT: regression-review.pdf

regression-review.pdf: regression-review.Rmd
	echo 'rmarkdown::render("$<", output_format="beamer_presentation")' | R --vanilla

%.pdf: %.md bibliography.bib
	pandoc --standalone --to=beamer --bibliography=$(refs) --from=markdown+raw_attribute+header_attributes+raw_html  --wrap=none $< -o $@

%.md: %.Rmd
	echo 'knitr::knit("$*.Rmd")' | R --vanilla

$(refs): bib.keys $(library)
ifeq ($(library),)
	@echo "No library specified, skipping generation of new bibliography"
else
	# if the content of bib.keys hasn't changed and the bibtex output exists, then
# don't bother running the extraction program; otherwise
# update the md5sum and extract
	(md5sum --check --status bib.keys.md5 && [ -f $(refs) ] ) || \
		(md5sum bib.keys > bib.keys.md5 && $(python) extractbib.py bib.keys $(library) $(refs))
endif

bib.keys: $(library) $(wildcard *.md)
	egrep '@[-:_a-zA-Z0-9.]*' *.md -oh --color=never | sort -u | sed 's/@//g' > bib.keys
